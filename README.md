# Titanic Dataset (K Nearest Neighbors)

### By: Andrew Wairegi

## Description - Titanic notebook
To be able to create a model that can predict whether a passenger will survive
on the boat cruise or not. Using K Nearest Neighbors. With an accuracy of atleast 80%. This will determine whether the 
model can predict if a passenger can survive or not using other cruises. This is the purpose of the notebook.

[Open notebook]

## Setup/Installation instructions
1. Create a folder on your computer
2. Set it up as a repository (using git init)
3. Clone this repository there (using git clone https://...)
4. Upload the notebooks to a google drive folder
5. Open it
6. Upload the data files for that collab in to the notebook (using the file upload section)
7. Execute the notebook

## Known Bugs
There are no known issues / bugs

## Technologies Used
1. Python - The programming language
2. Numpy - An Arithmetic Package
3. Pandas - A Data Analysis Package
4. Seaborn - A Visualization package
5. Matplotlib - A Visualization package
6. Scikit learn - A Modelling package

<br>

### License
Copyright © 2021 **Andrew Wairegi**
<br>
You are free to view, but not copy.

[Open notebook]: https://gitlab.com/d7713/machine-learning/KNN-Titanic_classification/-/blob/main/Titanic_dataset_(K_nearest_neighbors).ipynb
